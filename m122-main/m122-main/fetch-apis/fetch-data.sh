#!/bin/bash
echo "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8' /><meta name='viewport' content='width=device-width, initial-scale=1.0' /><title>F - API Calls</title><script src='https://cdn.tailwindcss.com'></script></head><body class='dark:bg-slate-900 text-white p-10 container mx-auto flex justify-center'><div>" > index.html

getCoinMarket() {
 echo "<h1 class='pb-5 font-medium text-4xl'>Coin Market</h1><table class='border-collapse border dark:border-slate-800 table-auto w-full text-sm'><thead><tr><th class='border-b dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'>Currency</th><th class='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'>Volume</th><th class='border-b dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'>Price</th><th class='border-b dark:border-slate-600 font-medium p-4 pr-8 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'>Percent Change</th></tr></thead><tbody class='bg-white dark:bg-slate-800'>" >> index.html
 url="https://pro-api.coinmarketcap.com/v1/cryptocurrency/listings/latest"
 api_key="1d1020c6-405b-47bb-8298-79bf0c3e32cf"
 params="start=1&limit=10&convert=USD"
 curl_cmd="curl -H \"X-CMC_PRO_API_KEY: $api_key\" -H \"Accept: application/json\" -d \"$params\" -G $url"
 response=$(eval $curl_cmd)
 data=$(echo "$response" | jq -r '.data')
 
 for row in $(echo "${data}" | jq -r '.[] | @base64'); do
    # Dekodiere den Base64-kodierten Eintrag
    _jq() {
     echo ${row} | base64 --decode | jq -r ${1}
    }

    # Greife auf die gewünschten Informationen zu und gib sie aus
    id=$(_jq '.id')
    name=$(_jq '.name')
    market_cap=$(_jq '.quote.USD.market_cap')
    price=$(_jq '.quote.USD.price')
    percent_change=$(_jq '.quote.USD.percent_change_1h')

    echo "<tr>" >> index.html
    echo "<td class='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>$name</td>" >> index.html
    echo "<td class='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>$market_cap</td>" >> index.html
    echo "<td class='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>$price</td>" >> index.html
    echo "<td class='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>$percent_change</td>" >> index.html
    echo "</tr>" >> index.html

 done
 echo "</table>" >> index.html
}

getCHFMarket() {
 echo "<h1 class='pt-5 pb-5 font-medium text-4xl'>CHF Exchanges</h1><table class='border-collapse border dark:border-slate-800 table-auto w-full text-sm'><thead><tr><th class='border-b dark:border-slate-600 font-medium p-4 pl-8 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'>Currency</th><th class='border-b dark:border-slate-600 font-medium p-4 pt-0 pb-3 text-slate-400 dark:text-slate-200 text-left'>Exchange rate</th></tr></thead>" >> index.html
 curl_cmd="curl -s https://open.er-api.com/v6/latest/CHF"
 response=$(eval $curl_cmd)
 data=$(echo "$response" | jq -r '.rates')
 
 for currency in $(echo "$data" |jq -r 'keys[]'); do
    rate=$(echo "$data" | jq -r ".[\"$currency\"]")
    echo "<tr>" >> index.html
    echo "<td class='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>$currency</td>" >> index.html
    echo "<td class='border-b border-slate-100 dark:border-slate-700 p-4 pl-8 text-slate-500 dark:text-slate-400'>$rate</td>" >> index.html
    echo "</tr>" >> index.html
 done
 echo "</table>" >> index.html
}
 
getQuote() { 
 echo "<h1 class='pt-5 pb-5 font-medium text-4xl'>Quote of the Day<h1>" >> index.html
 curl_cmd="curl -s https://api.quotable.io/quotes/random"
 response=$(eval $curl_cmd)
 data=$(echo "$response" | jq -r '.[0].content')
 echo "<p class='text-lg'>- $data</p>" >> index.html

}

getCoinMarket
getQuote
getCHFMarket

echo "</div></body></html>" >> index.html
