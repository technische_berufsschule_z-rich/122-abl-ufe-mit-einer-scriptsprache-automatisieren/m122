file_path="index.html"
file_content=$(base64 -w 0 "$file_path")

curl --request POST \
  --url https://api.sendgrid.com/v3/mail/send \
  --header "Authorization: Bearer $SENDGRID_API_KEY" \
  --header 'Content-Type: application/json' \
  --data '{"personalizations": [{"to": [{"email": "abinayansureskumar@gmail.com"}]}],"from": {"email": "abi@tie-international.com"},"subject": "F: APIs","content": [{"type": "text/plain", "value": "To easy to do anywhere, even with cURL"}], "attachments": [ { "content": "'"$file_content"'", "filename": "'"$file_path"'" } ]}'
