#!/bin/bash

hostname=$(hostname)
os_version=$(cat /etc/os-release | grep "PRETTY_NAME" | cut -d'"' -f2)
cpu_model=$(lscpu | grep "Model name:" | cut -d':' -f2 | awk '{$1=$1};1')
cpu_cores=$(lscpu | grep "CPU(s):" | head -n1 | awk '{print $2}')
total_memory=$(free -h --si | grep "Mem:" | awk '{print $2}')
used_memory=$(free -h --si | grep "Mem:" | awk '{print $3}')
available_memory=$(free -h --si | grep "Mem:" | awk '{print $7}')
free_memory=$(free -h --si | grep "Mem:" | awk '{print $4}')
total_disk_size=$(df -h --si / | tail -n1 | awk '{print $2}')
used_disk_space=$(df -h --si / | tail -n1 | awk '{print $3}')
free_disk_space=$(df -h --si / | tail -n1 | awk '{print $4}')
uptime=$(uptime -p)
current_time=$(date +'%Y-%m-%d %H:%M:%S')
file_name="$hostname.sys-info"

echo -e "\033[0;35m============================== \033[0;37mSystem Info \033[0;35m==============================\033[0m" 
echo -e "Hostname:              $hostname"
echo -e "Operating System:      $os_version"
echo -e "CPU:                   $cpu_model"
echo -e "Number of Cores:       $cpu_cores"
echo -e "Memory:                $total_memory"
echo -e "Available Memory:      $available_memory"
echo -e "Used Memory:           $used_memory"
echo ""
echo -e "Free Memory:           $free_memory"
echo -e "File System:           $total_disk_size"
echo -e "Used disk space:       $used_disk_space"
echo -e "Free disk space:       $free_disk_space"
echo -e "Uptime:                $uptime"
echo -e "Current time:          $current_time"
echo -e "\033[0;35m=========================================================================\033[0m"


echo "============================== System Info ==============================" > "$file_name"
echo "Hostname:              $hostname" >> "$file_name"
echo "Operating System:      $os_version" >> "$file_name"
echo "CPU:                   $cpu_model" >> "$file_name"
echo "Number of Cores:       $cpu_cores" >> "$file_name"
echo "Memory:                $total_memory" >> "$file_name"
echo "Available Memory:      $available_memory" >> "$file_name"
echo "Used Memory:           $used_memory" >> "$file_name"
echo "" >> "$file_name"
echo "Free Memory:           $free_memory" >> "$file_name"
echo "File System:           $total_disk_size" >> "$file_name"
echo "Used disk space:       $used_disk_space" >> "$file_name"
echo "Free disk space:       $free_disk_space" >> "$file_name"
echo "Uptime:                $uptime" >> "$file_name"
echo "Current time:          $current_time" >> "$file_name"
echo "=========================================================================" >> "$file_name"

echo "<!DOCTYPE html><html lang='en'><head><meta charset='UTF-8' /><meta name='viewport' content='width=device-width, initial-scale=1.0' /><title>E - System Information</title><script src='https://cdn.tailwindcss.com'></script></head><body class='bg-slate-900 text-white p-10 container mx-auto flex justify-center items-center'><div class='bg-slate-800 p-3 px-10 rounded-lg w-full'><div>" > index.html
echo "<h1 class='text-3xl text-center font-bold'>System Information</h1>" >> index.html
echo "<p class='mt-5'>Hostname: $hostname</p>" >> index.html
echo "<p class='mt-1'>Operating System: $os_version</p>" >> index.html
echo "<p class='mt-1'>CPU: $cpu_model</p>" >> index.html
echo "<p class='mt-1'>Number of Cores: $cpu_cores</p>" >> index.html
echo "<p class='mt-1'>Memory: $total_memory</p>" >> index.html
echo "<p class='mt-1'>Available Memory: $available_memory</p>" >> index.html
echo "<p class='mt-1'>Used Memory: $used_memory</p>" >> index.html

echo "<p class='mt-5'>Free Memory: $free_memory</p>" >> index.html
echo "<p class='mt-1'>File System: $total_disk_size</p>" >> index.html
echo "<p class='mt-1'>Used disk space: $used_memory</p>" >> index.html
echo "<p class='mt-1'>Free disk space: $free_disk_space</p>" >> index.html
echo "<p class='mt-1'>Uptime: $uptime</p>" >> index.html
echo "<p class='mt-1'>Current time: $current_time</p>" >> index.html

echo "</div></div></body></html>" >> index.html
