file_path="archive/2023-06-09_10-42-17_newMails_AP22d.tar.gz"
file_content=$(base64 -w 0 "$file_path")

curl --request POST \
  --url https://api.sendgrid.com/v3/mail/send \
  --header "Authorization: Bearer $SENDGRID_API_KEY" \
  --header 'Content-Type: application/json' \
  --data '{"personalizations": [{"to": [{"email": "abinayansureskumar@gmail.com"}]}],"from": {"email": "abi@tie-international.com"},"subject": "Sending with SendGrid is Fun","content": [{"type": "text/plain", "value": "and easy to do anywhere, even with cURL"}], "attachments": [ { "content": "'"$file_content"'", "filename": "'"$file_path"'" } ]}'
