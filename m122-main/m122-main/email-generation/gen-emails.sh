csv_file="MOCK_DATA.csv"

rm -rfd "output"
rm -rfd "archive"

mkdir "output"
mkdir "archive"
output_file="output/$(date +'%Y-%m-%d_%H-%M')_mailimports.csv"
output_tar="archive/$(date +'%Y-%m-%d_%H-%M-%S_newMails_AP22d').tar.gz"

tail -n +2 "$csv_file" | while IFS="," read -r id Vorname Nachname Geschl Strasse StrNummer Postleitzahl Ort; do
  vorname=$(echo "$Vorname" | tr -d ' ' | tr '[:upper:]' '[:lower:]' | iconv -f utf8 -t ascii//TRANSLIT)
  nachname=$(echo "$Nachname" | tr -d ' ' | tr '[:upper:]' '[:lower:]' | iconv -f utf8 -t ascii//TRANSLIT)
  email="${vorname}.${nachname}@edu.tbz.ch"
  password=$(openssl rand -base64 12 | tr -dc 'a-zA-Z0-9' | head -c 8)
  echo "$email;$password" >> "$output_file"

  output_letter="output/$(date +'%Y-%m-%d_%H-%M')_${email}.brf"
  echo "Technische Berufsschule Zürich" >> "$output_letter"
  echo "Ausstellungsstrasse 70" >> "$output_letter"
  echo "8005 Zürich" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Zürich, den $(date +'%d.%m.%Y')" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "                                               $Vorname $Nachname" >> "$output_letter"
  echo "                                               $Strasse $StrNummer" >> "$output_letter"
  echo "                                               $Postleitzahl $Ort" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Liebe:r $Vorname" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Es freut uns, Sie im neuen Schuljahr begrüssen zu dürfen." >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Damit Sie am ersten Tag sich in unsere System einloggen" >> "$output_letter"
  echo "können, erhalten Sie hier Ihre neue Emailadresse und Ihr" >> "$output_letter"
  echo "Initialpasswort, das Sie beim ersten Login wechseln müssen." >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Emailadresse:      $email" >> "$output_letter"
  echo "Password:          $password" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Mit freundlichen Grüssen" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "Abinayan Sureskumar" >> "$output_letter"
  echo "(TBZ-IT-Service)" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "" >> "$output_letter"
  echo "admin.it@tbz.ch, Abt. IT: +41 44 446 96 60" >> "$output_letter"
done

chmod +x "output"
tar czvf "$output_tar" output/*
